package me.jeremy.connectthree;

import android.widget.RelativeLayout;

public class BoardCell {
    private RelativeLayout viewCell;
    private String color;

    public final static String RED = "Red";
    public final static String BLUE = "Blue";
    public final static String LEFT_BOTTOM = "leftBottom";
    public final static String LEFT_CENTER = "leftCenter";
    public final static String LEFT_TOP = "leftTop";
    public final static String CENTER_BOTTOM = "centerBottom";
    public final static String CENTER_CENTER = "centerCenter";
    public final static String CENTER_TOP = "centerTop";
    public final static String RIGHT_BOTTOM = "rightBottom";
    public final static String RIGHT_CENTER = "rightCenter";
    public final static String RIGHT_TOP = "rightTop";

    public RelativeLayout getViewCell() {
        return viewCell;
    }

    public void setViewCell(RelativeLayout viewCell) {
        this.viewCell = viewCell;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public BoardCell() {
    }
}
