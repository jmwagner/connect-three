package me.jeremy.connectthree;

import android.widget.ImageView;

public class BoardCellTO {
    private String color;
    private ImageView coin;
    private String nextTurn;

    public BoardCellTO() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public ImageView getCoin() {
        return coin;
    }

    public void setCoin(ImageView coin) {
        this.coin = coin;
    }

    public String getNextTurn() {
        return nextTurn;
    }

    public void setNextTurn(String nextTurn) {
        this.nextTurn = nextTurn;
    }
}
