package me.jeremy.connectthree;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Map<String,BoardCell> boardCellMap;
    private boolean isBlueTurn;
    private final float COIN_OFFSET = 1000f;
    private final int COIN_DURATION = 500;
    private final int COIN_SIDE = 400;

    public boolean isBlueTurn() {
        return isBlueTurn;
    }

    public void setBlueTurn(boolean blueTurn) {
        isBlueTurn = blueTurn;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        boardCellMap = getEmptyBoard();
    }

    public void resetBoard(View view){
        for(BoardCell cell : boardCellMap.values()){
            cell.getViewCell().removeAllViews();
            cell.setColor(null);
        }
        setBlueTurn(Math.random() > 0.5);
        TextView output = findViewById(R.id.output);
        String clr = isBlueTurn ? BoardCell.BLUE : BoardCell.RED;
        String textOutput = clr + " team's turn";
        output.setText(textOutput);
    }

    private BoardCellTO prepareCoin() {
        BoardCellTO to = new BoardCellTO();
        ImageView nextCoin = new ImageView(this);

        String clr;
        String nextTurn;
        if(isBlueTurn()){
            nextCoin.setImageResource(R.drawable.blue);
            clr = BoardCell.BLUE;
            nextTurn = BoardCell.RED;
            setBlueTurn(false);
        } else {
            nextCoin.setImageResource(R.drawable.red);
            clr = BoardCell.RED;
            nextTurn = BoardCell.BLUE;
            setBlueTurn(true);
        }

        to.setCoin(nextCoin);
        to.setColor(clr);
        to.setNextTurn(nextTurn);
        return to;
    }

    private void dropCoin(BoardCell cell, BoardCellTO to){
        RelativeLayout.LayoutParams coinParams = new RelativeLayout.LayoutParams(COIN_SIDE,COIN_SIDE);
        coinParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        coinParams.addRule(RelativeLayout.CENTER_VERTICAL);

        if(cell != null){
            RelativeLayout v = cell.getViewCell();
            cell.setColor(to.getColor());
            v.setTranslationY(-COIN_OFFSET);
            v.addView(to.getCoin(),coinParams);
            v.animate().translationYBy(COIN_OFFSET).setDuration(COIN_DURATION);
            TextView output = findViewById(R.id.output);
            String textOutput = to.getNextTurn() + " team's turn";
            output.setText(textOutput);
            determineGameState();
        }
    }

    public void putLeft(View view){
        BoardCellTO to = prepareCoin();

        BoardCell cell = null;
        if(boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor() == null){
            cell = boardCellMap.get(BoardCell.LEFT_BOTTOM);
        } else if (boardCellMap.get(BoardCell.LEFT_CENTER).getColor() == null){
            cell = boardCellMap.get(BoardCell.LEFT_CENTER);
        } else if (boardCellMap.get(BoardCell.LEFT_TOP).getColor() == null){
            cell = boardCellMap.get(BoardCell.LEFT_TOP);
        }

        dropCoin(cell,to);
    }

    public void putCenter(View view){
        BoardCellTO to = prepareCoin();

        BoardCell cell = null;
        if(boardCellMap.get(BoardCell.CENTER_BOTTOM).getColor() == null){
            cell = boardCellMap.get(BoardCell.CENTER_BOTTOM);
        } else if (boardCellMap.get(BoardCell.CENTER_CENTER).getColor() == null){
            cell = boardCellMap.get(BoardCell.CENTER_CENTER);
        } else if (boardCellMap.get(BoardCell.CENTER_TOP).getColor() == null){
            cell = boardCellMap.get(BoardCell.CENTER_TOP);
        }

        dropCoin(cell, to);
    }

    public void putRight(View view){
        BoardCellTO to = prepareCoin();

        BoardCell cell = null;
        if(boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor() == null){
            cell = boardCellMap.get(BoardCell.RIGHT_BOTTOM);
        } else if (boardCellMap.get(BoardCell.RIGHT_CENTER).getColor() == null){
            cell = boardCellMap.get(BoardCell.RIGHT_CENTER);
        } else if (boardCellMap.get(BoardCell.RIGHT_TOP).getColor() == null){
            cell = boardCellMap.get(BoardCell.RIGHT_TOP);
        }

        dropCoin(cell, to);
    }

    private Map<String,BoardCell> getEmptyBoard(){
        Map<String,BoardCell> boardCellMap = new HashMap<>();
        BoardCell leftBottom = new BoardCell();
        leftBottom.setViewCell((RelativeLayout)findViewById(R.id.leftBottomView));
        boardCellMap.put(BoardCell.LEFT_BOTTOM,leftBottom);

        BoardCell leftCenter = new BoardCell();
        leftCenter.setViewCell((RelativeLayout)findViewById(R.id.leftCenterView));
        boardCellMap.put(BoardCell.LEFT_CENTER,leftCenter);

        BoardCell leftTop = new BoardCell();
        leftTop.setViewCell((RelativeLayout)findViewById(R.id.leftTopView));
        boardCellMap.put(BoardCell.LEFT_TOP,leftTop);

        BoardCell centerBottom = new BoardCell();
        centerBottom.setViewCell((RelativeLayout)findViewById(R.id.centerBottomView));
        boardCellMap.put(BoardCell.CENTER_BOTTOM,centerBottom);

        BoardCell centerCenter = new BoardCell();
        centerCenter.setViewCell((RelativeLayout)findViewById(R.id.centerCenterView));
        boardCellMap.put(BoardCell.CENTER_CENTER,centerCenter);

        BoardCell centerTop = new BoardCell();
        centerTop.setViewCell((RelativeLayout)findViewById(R.id.centerTopView));
        boardCellMap.put(BoardCell.CENTER_TOP,centerTop);

        BoardCell rightBottom = new BoardCell();
        rightBottom.setViewCell((RelativeLayout)findViewById(R.id.rightBottomView));
        boardCellMap.put(BoardCell.RIGHT_BOTTOM,rightBottom);

        BoardCell rightCenter = new BoardCell();
        rightCenter.setViewCell((RelativeLayout)findViewById(R.id.rightCenterView));
        boardCellMap.put(BoardCell.RIGHT_CENTER,rightCenter);

        BoardCell rightTop = new BoardCell();
        rightTop.setViewCell((RelativeLayout)findViewById(R.id.rightTopView));
        boardCellMap.put(BoardCell.RIGHT_TOP,rightTop);

        setBlueTurn(Math.random() > 0.5);
        TextView output = findViewById(R.id.output);
        String clr = isBlueTurn ? BoardCell.BLUE : BoardCell.RED;
        String textOutput = clr + " team's turn";
        output.setText(textOutput);

        return boardCellMap;

    }

    private void determineGameState(){
        String winner = null;

        //Left Column
        if(boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor() != null && boardCellMap.get(BoardCell.LEFT_CENTER).getColor() != null && boardCellMap.get(BoardCell.LEFT_TOP).getColor() != null
            && boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.LEFT_CENTER).getColor()) && boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.LEFT_TOP).getColor())){
            winner = boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor();
        }

        //Center Column
        if(boardCellMap.get(BoardCell.CENTER_BOTTOM).getColor() != null && boardCellMap.get(BoardCell.CENTER_CENTER).getColor() != null && boardCellMap.get(BoardCell.CENTER_TOP).getColor() != null
                && boardCellMap.get(BoardCell.CENTER_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.CENTER_CENTER).getColor()) && boardCellMap.get(BoardCell.CENTER_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.CENTER_TOP).getColor())){
            winner = boardCellMap.get(BoardCell.CENTER_BOTTOM).getColor();
        }

        //Right Column
        if(boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor() != null && boardCellMap.get(BoardCell.RIGHT_CENTER).getColor() != null && boardCellMap.get(BoardCell.RIGHT_TOP).getColor() != null
                && boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.RIGHT_CENTER).getColor()) && boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.RIGHT_TOP).getColor())){
            winner = boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor();
        }

        //BOTTOM ROW
        if(boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor() != null && boardCellMap.get(BoardCell.CENTER_BOTTOM).getColor() != null && boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor() != null
                && boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.CENTER_BOTTOM).getColor()) && boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor().equals(boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor())){
            winner = boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor();
        }

        //CENTER ROW
        if(boardCellMap.get(BoardCell.RIGHT_CENTER).getColor() != null && boardCellMap.get(BoardCell.CENTER_CENTER).getColor() != null && boardCellMap.get(BoardCell.LEFT_CENTER).getColor() != null
                && boardCellMap.get(BoardCell.RIGHT_CENTER).getColor().equals(boardCellMap.get(BoardCell.CENTER_CENTER).getColor()) && boardCellMap.get(BoardCell.RIGHT_CENTER).getColor().equals(boardCellMap.get(BoardCell.LEFT_CENTER).getColor())){
            winner = boardCellMap.get(BoardCell.RIGHT_CENTER).getColor();
        }

        //TOP ROW
        if(boardCellMap.get(BoardCell.RIGHT_TOP).getColor() != null && boardCellMap.get(BoardCell.CENTER_TOP).getColor() != null && boardCellMap.get(BoardCell.LEFT_TOP).getColor() != null
                && boardCellMap.get(BoardCell.RIGHT_TOP).getColor().equals(boardCellMap.get(BoardCell.CENTER_TOP).getColor()) && boardCellMap.get(BoardCell.RIGHT_TOP).getColor().equals(boardCellMap.get(BoardCell.LEFT_TOP).getColor())){
            winner = boardCellMap.get(BoardCell.RIGHT_TOP).getColor();
        }

        //BACKSLASH
        if(boardCellMap.get(BoardCell.LEFT_TOP).getColor() != null && boardCellMap.get(BoardCell.CENTER_CENTER).getColor() != null && boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor() != null
                && boardCellMap.get(BoardCell.LEFT_TOP).getColor().equals(boardCellMap.get(BoardCell.CENTER_CENTER).getColor()) && boardCellMap.get(BoardCell.LEFT_TOP).getColor().equals(boardCellMap.get(BoardCell.RIGHT_BOTTOM).getColor())){
            winner = boardCellMap.get(BoardCell.LEFT_TOP).getColor();
        }

        //SLASH
        if(boardCellMap.get(BoardCell.RIGHT_TOP).getColor() != null && boardCellMap.get(BoardCell.CENTER_CENTER).getColor() != null && boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor() != null
                && boardCellMap.get(BoardCell.RIGHT_TOP).getColor().equals(boardCellMap.get(BoardCell.CENTER_CENTER).getColor()) && boardCellMap.get(BoardCell.RIGHT_TOP).getColor().equals(boardCellMap.get(BoardCell.LEFT_BOTTOM).getColor())){
            winner = boardCellMap.get(BoardCell.RIGHT_TOP).getColor();
        }

        if(winner != null){
            TextView output = findViewById(R.id.output);
            output.setText(winner + " team wins!");
        } else {
            boolean isFull = true;
            for(BoardCell cell : boardCellMap.values()){
                if(cell.getColor() == null){
                    isFull = false;
                    break;
                }
            }

            if(isFull){
                TextView output = findViewById(R.id.output);
                output.setText("NO MORE MOVES");
            }
        }
    }
}
